const express = require('express');
const uuidv4 = require('uuid/v4');
const cors = require('cors');
const MaxstBuilder = require('../schemas/maxstbuilder');

const router = express.Router();

router.use(cors());
router.get('/', (req, res) => {
    res.render('index.html');
    // res.sendFile('./index.html');
});
router.get('/api', (req, res, next) => {
    console.log(req)
    MaxstBuilder.findOne({ })
        .then((data) => {
            res.send(data.data);
        })
        .catch((error) => {
            console.error(error);
            res.send(error);
            next(error);
        });
});

router.post('/api', (req, res, next) => {
    console.log(req)
    MaxstBuilder.remove({})
        .then(() => {
            const builder = new MaxstBuilder({
                data: JSON.stringify(req.body)
            });
            return builder.save();
        })
        .then((result) => {
            res.status(201).json(result);
        })
        .catch((err) => {
            console.error('error', err);
            res.json({result: 0});
            next(err);
        });
});

module.exports = router;
