const mongoose = require('mongoose');

const { Schema } = mongoose;
const maxstSchema = new Schema({
    data: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('3d_builder', maxstSchema);
