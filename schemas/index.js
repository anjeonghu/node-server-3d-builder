const mongoose = require('mongoose');

module.exports = () => {
  const connect = () => {

    const uri = "mongodb+srv://jhan:maxst2010@cluster0.yvkw4.mongodb.net/maxst?retryWrites=true&w=majority";
// DB setting
    mongoose.set('useNewUrlParser', true);    // 1
    mongoose.set('useFindAndModify', false);  // 1
    mongoose.set('useCreateIndex', true);     // 1
    mongoose.set('useUnifiedTopology', true); // 1

    if (process.env.NODE_ENV !== 'production') {
      mongoose.set('debug', true);
    }
    mongoose.connect(uri, {
      dbName: 'maxst',
    }, (error) => {
      if (error) {
        console.log('몽고디비 연결 에러', error);
      } else {
        console.log('몽고디비 연결 성공');
      }
    });
  };
  connect();
  mongoose.connection.on('error', (error) => {
    console.error('몽고디비 연결 에러', error);
  });
  mongoose.connection.on('disconnected', () => {
    console.error('몽고디비 연결이 끊겼습니다. 연결을 재시도합니다.');
    connect();
  });

  require('./maxstbuilder');
};
